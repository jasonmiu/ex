#!/usr/bin/env python3

import sys

def add_list(a, b):
    if len(a) == 0 and len(b) == 0:
        return [0]
    
    if len(a) < len(b):
        a, b = b, a

    d = len(a) - len(b)
    b = ([0] * d) + b

    carry = 0
    c = []
    for i in range(len(a) - 1, -1, -1):
        r = (a[i] + b[i]) % 10
        s = r + carry
        if s >= 10:
            c.append(s % 10)
        else:
            c.append(s)

        carry = (a[i] + b[i] + carry) // 10

        print("i", i, "carry", carry)


    if carry:
        c.append(carry)


    c.reverse()
    print(c)

    return c
        

def main():
    
    list1 = list(map(int, sys.argv[1].strip().split()))
    list2 = list(map(int, sys.argv[2].strip().split()))

    print("L1", list1)
    print("L2", list2)

    add_list(list1, list2)


if __name__ == "__main__":
    sys.exit(main())
