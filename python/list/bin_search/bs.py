#!/usr/bin/env python3

import sys

def main():

    if len(sys.argv) != 3:
        print("Return the index of target in the sorted list. Elements are compared with dictionary order")
        print("USAGE: {} <spare sperated list> <target>".format(sys.argv[0]))
        return 1
    
    arr = sys.argv[1].split()
    target = sys.argv[2]

    l = 0
    h = len(arr) - 1
    ans = None
    
    while not l > h:
        m = ((h - l) // 2) + l
        if arr[m] == target:
            ans = m
            break
        if arr[m] < target:
            l = m + 1
        else:
            h = m - 1

    print(ans)

    return 0
    
    

if __name__ == "__main__":
    sys.exit(main())
