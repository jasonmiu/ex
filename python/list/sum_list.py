#!/usr/bin/env python3

# Create a Linked List from a list
# Sum two linked list as each node of a linked list
# is a digit. The digits are in reversed order.
# For example:
# 0 -> 1 -> 3
# 5 -> 9
# => 310 + 95 = 405
# 5 -> 0 -> 4

import sys

class Node(object):
    def __init__(self, val):
        self.val = val
        self.next = None

class LinkedList(object):
    def __init__(self, array):
        self.head = None
        p = None
        
        for i in range(0, len(array)):
            n = Node(array[i])
            if i == 0:
                self.head = n

            if p != None:
                p.next = n

            p = n

        self.tail = p

    def append(self, val):
        n = Node(val)
        if not self.head:
            self.head = n
        else:
            self.tail.next = n
                
        self.tail = n

    def print(self):
        n = self.head
        while n:
            print(n.val, "", end="")
            n = n.next
        print("")

def sum_linkedlist(l1, l2):
    n1 = l1.head
    n2 = l2.head

    ans = LinkedList([])

    carry = 0
    while n1 != None or n2 != None:
        v = 0

        if n1 and n2:
            v = n1.val + n2.val + carry
        elif n1 == None:
            v = n2.val + carry
        elif n2 == None:
            v = n1.val + carry

        if v >= 10:
            v = v - 10
            carry = 1
        else:
            carry = 0

        ans.append(v)

        if n1:
            n1 = n1.next
        if n2:
            n2 = n2.next

    if carry:
        ans.append(1)

    return ans
        

def main():
    a1 = list(map(int, sys.argv[1].split()))
    a2 = list(map(int, sys.argv[2].split()))
    
    l1 = LinkedList(a1)
    print("L1")
    l1.print()
    print("L1 append 99")
    l1.append(99)
    l2 = LinkedList([])
    print("L2 EMPTY")
    l2.print()
    print("L2 append")
    l2.append(99)
    l2.print()
    print("L2 append")
    l2.append(123)
    l2.print()

    print("Sum", a1, a2)
    s = sum_linkedlist(LinkedList(a1), LinkedList(a2))
    s.print()

if __name__ == "__main__":
    sys.exit(main())
