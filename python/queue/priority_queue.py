import heapq

class PriorityQueue(object):
    REMOVED = "__removed__"
    def __init__(self):
        self.q = []
        self.entry_finder = dict()

    def add(self, key, priority):
        if key in self.entry_finder:
            self.remove(key)

        entry = [priority, key]
        self.entry_finder[key] = entry
        heapq.heappush(self.q, entry)

    def remove(self, key):
        e = self.entry_finder.pop(key)
        e[-1] = PriorityQueue.REMOVED

    def pop(self):
        while self.q:
            (priority, key) = heapq.heappop(self.q)
            if key != PriorityQueue.REMOVED:
                return key

    def __len__(self):
        return len(self.q)

    def __contains__(self, x):
        if x in self.entry_finder:
            return True
        else:
            return False
