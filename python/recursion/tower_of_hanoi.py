#!/usr/bin/env python3

def tower(n, from_p, aux_p, to_p):
    if n == 1:
        print("Move disk 1 from {} to {}".format(from_p, to_p))
    else:
        tower(n-1, from_p, to_p, aux_p)
        print("Move disk {} from {} to {}".format(n, from_p, to_p))
        tower(n-1, aux_p, from_p, to_p)
    

import sys
def main():
    n = int(sys.argv[1])

    tower(n, "A", "B", "C")


main()
    
