#!/usr/bin/env python3

### Ref:
### https://www.geeksforgeeks.org/sliding-window-maximum-maximum-of-all-subarrays-of-size-k/

import collections
import sys

# Return the index of the min of each window
def sliding_window_mins(nums, window_size):
    k = window_size
    # The deque head is the index min of the current window
    # The deque tail is the index max of the current window
    q = collections.deque()
    result = []

    for r in range(0, len(nums)):
        val = nums[r]
        # the current val is smaller than the
        # last element in the q, remove the tail
        # element of the q as it cannot be min in this
        # window anymore
        while len(q) > 0 and val < nums[q[-1]]:
            p = q.pop()

        q.append(r)

        # r is the right index of window, l is the left index of window
        l = r - k + 1
        if l >= 0:
            # we shifted window 1 unit to right, the current stored index
            # at the queue head is the min of the current window
            result.append(q[0])
            
            # The index stored in queue head is going out of the current window,
            # hence no longer useful, remove it
            if q[0] <= l:
                q.popleft()

    return result

def list_from_indices(nums, indices):
    return [nums[i] for i in indices]
    

def main():

    if len(sys.argv) != 2:
        print("{} <space separated int list> <window size K>".format(sys.argv[0]))
        print("Find the min value in the sliding window of the input list")
        print("Formal: Given an array and an integer K, find the min for each and every contiguous subarray of size k.")
    
    nums = list(map(int, sys.argv[1].split(" ")))
    k = int(sys.argv[2])

    result = sliding_window_mins(nums, k)
    print(list_from_indices(nums, result))
    

if __name__ == "__main__":
    sys.exit(main())
