#!/usr/bin/env python3

# Giving two input:
# input string
# format dictionary
# Add the HTLM tag at the indices of the input string according to the format
# dictionary.
# The format dictionary looks like:
#
# {
#     "bold" : [[0, 2]],
#     "italic" : [[3, 4]]
#     "underline" : [[5, 6]
# }
# The key is the tag type. The value is a list of 2-tuples,
# each tuple is a the starting index and end index + 1
# (the end index is exclusive).
#
# The hard part is, if two tag types are overlapping, like:
#
# bold: [0, 2]
# italic: [1, 4]
#
#  III
# BB 
# 012345
# ABCDEF
#
# The output should be:
# <b>A<i>B</i><b><i>CD</i>EF
# Before closing the </b>, we need to close the interleave </i>
# at index 1



import sys
import json
import collections

class Op(object):
    B_OPEN = 0
    B_CLOSE = 1
    I_OPEN = 3
    I_CLOSE = 4
    U_OPEN = 6
    U_CLOSE = 7

    fmap = {
        B_OPEN : "<b>",
        B_CLOSE : "</b>",
        I_OPEN : "<i>",
        I_CLOSE : "</i>",
        U_OPEN : "<u>",
        U_CLOSE : "</u>"
        }
    
    def get_tag(op):
        return Op.fmap[op]

    def is_open(op):
        if op == Op.B_OPEN or op == Op.I_OPEN or op == Op.U_OPEN:
            return True
        else:
            return False

    def is_pair(op1, op2):
        if abs(op2 - op1) == 1:
            return True
        else:
            return False
            

def convert_to_ops(format_dict):
    ops = collections.defaultdict(list)
    for (k, v) in format_dict.items():
        for t in v:
            s = t[0]
            e = t[1]

            if k == "bold":
                ops[s].append(Op.B_OPEN)
                ops[e].append(Op.B_CLOSE)

            elif k == "italic":
                ops[s].append(Op.I_OPEN)
                ops[e].append(Op.I_CLOSE)

            elif k == "underline":
                ops[s].append(Op.U_OPEN)
                ops[e].append(Op.U_CLOSE)

    return ops


def format_str(s, ops):

    output = ""
    stack = []
    for i in range(0, len(s)+1):
        if i in ops:
            for op in ops[i]:
                # Open Tag. Just create a tag and put it on stack
                if Op.is_open(op):
                    output = output + Op.get_tag(op)
                    stack.append(op)
                else:
                    q = []
                    s2 = []
                    # This is a close tag. Check if the current top of
                    # stack is the same type of this close tag
                    top = stack.pop(-1)
                    q.append(top)
                    while(not Op.is_pair(top, op)):
                        top = stack.pop(-1)
                        q.append(top)

                    # We put all pop-ed elements to a queue, so it is
                    # the reverse of the sequence. For example:
                    # Stack : [ B_OPEN, I_OPEN ], current op: B_CLOSE
                    # the queue become:
                    # Stack : [], q : [I_OPEN, B_OPEN]
                    # We like to close the interleave tags first.
                    # We want to re-open those interleave tags right after
                    # we closed all current tags, so we reverse back it to
                    # another stack s2
                    # s2 : [ I_OPEN ]
                        
                    while(len(q) != 0):
                        qh = q.pop(0)
                        output = output + Op.get_tag(qh+1)
                        if not Op.is_pair(qh, op):
                            s2.append(qh)

                    # We want to re-open the tags and put it back to the
                    # main stack
                    while(len(s2) != 0):
                        st = s2.pop(-1)
                        stack.append(st)
                        output = output + Op.get_tag(st)

        if i < len(s):
            output += s[i]

                        
    return output
                    
            
    
def main():
    input_str = sys.argv[1]
    f = sys.argv[2]
    format_dict = None
    with open(f, 'r') as fd:
        s = fd.read()
        format_dict = json.loads(s)
        
    ops = convert_to_ops(format_dict)

    out = format_str(input_str, ops)

    print(out)



if __name__ == "__main__":
    sys.exit(main())
