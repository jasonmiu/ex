#!/usr/bin/env python3

'''
Find the next greater number of each element in an array. If there is
no next greater number, the value is -1.

For example:
input = [2, 1, 2, 4, 3]
ans   = [4, 2, 4, -1, -1]

For input[0] == 2, the next greater number if input[3] == 4. 
For input[3] == 4, there is no next greater number, so ans[3] is -1.
'''


import sys


# the idea is, if we look the list from behide,
# if we need a value is bigger than the current
# max, we should replace the current max since it will
# be the next greater value of the value on the left.
# We want to find the saved values which is bigger than the current value,
# so we pop up the stack which means we scan the saved values from
# right to left since stack is last in (left most values) first out.
def next_greater(nums):
    mono_stack = [] # mono decrease stack
    ans = [0] * len(nums)
    for i in range(len(nums)-1, -1, -1):
        n = nums[i]
        print("i", i, "n", n, "stack", mono_stack)

        while (len(mono_stack) > 0) and (n >= mono_stack[-1]):
            mono_stack.pop()

        if len(mono_stack) == 0:
            ans[i] = -1
        else:
            ans[i] = mono_stack[-1]

        mono_stack.append(n)
    
    return ans

        
        

def main():

    if len(sys.argv) != 2:
        print("For each number i, find the next number on the right which is greater than i")
        print("USAGE: {} <space seperated numbers>".format(sys.argv[0]))
        return -1
    
    nums = list(map(int, sys.argv[1].split()))
    print(nums)

    ans = next_greater(nums)
    print(ans)
    


if __name__ == "__main__":
    sys.exit(main())
