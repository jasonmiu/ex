#!/usr/bin/env python3
import sys

class MaxHeap(object):
    def __init__(self, a=[]):
        self.array = a
        self.heapify(a)

    # For a heap, it is a full bin tree.
    # Can be reprsented in an array.
    # left child of A[i] is A[i*2 + 1]
    # right child of A[i] is A[i*2 + 2]
    # example:
    #       10
    #      /  \
    #     6    8
    #    / \
    #   4   5
    # can be represented in:
    # [10, 6, 8, 4, 5]
    # In a max heap, for each node, it's value
    # must the largest of its left and right subtrees.
    # Hence, for a node, it must be the largest of it
    # left and right child nodes. Have this relation
    # downward recursively.

    def heapify_at(self, a, i):
        n = len(a)
        l_idx = (i * 2) + 1
        r_idx = (i * 2) + 2

        max_idx = i
        if l_idx < n and a[l_idx] > a[i]:
            max_idx = l_idx
            
        if r_idx < n and a[r_idx] > a[max_idx]:
            max_idx = r_idx

        if max_idx != i:
            t = a[i]
            a[i] = a[max_idx]
            a[max_idx] = t
            return self.heapify_at(a, max_idx)

        return a

    def heapify(self, x=[]):

        a = x
        n = len(a)
        # Float the larger node upward for each node.
        # Start from n//2 as this is the hight - 1 level
        # of the tree. Do not need to start from leaves
        # as its parent node will compare them and float them
        # up if they are the larger one.
        # So n/2 * logn = O(n)
        for i in range(n//2, -1, -1):
            a = self.heapify_at(a, i)

        self.array = a

    def pop(self):
        m = self.array[0]
        n = len(self.array)
        self.array[0] = self.array[n-1]
        del self.array[n-1]
        self.heapify_at(self.array, 0)
        return m


    def __str__(self):
        return str(self.array)



    
if __name__ == "__main__":

    # a = [4, 1, 3, 2, 16, 9, 10, 14, 8, 7]
    # return: [16, 14, 10, 8, 7, 9, 3, 2, 4, 1]

    if len(sys.argv) != 2:
        print("Do a max heap")
        print("USAGE: {} <a string numbers sperated by spaces>".format(sys.argv[0]))
        sys.exit(-1)
    
    a = list(map(int, sys.argv[1].split()))
    h = MaxHeap(a)
    print(h)

    for i in range(0, len(a)):
        print("Pop", h.pop())
        
