#!/usr/bin/env python3

import sys
import json

def dfs_preorder(root):
    stack = []
    stack.append(root)
          
    while len(stack) > 0:
        n = stack.pop()
        print(n["val"])

        if n["right"]:
            stack.append(n["right"])

        if n["left"]:
            stack.append(n["left"])

def dfs_inorder(root):
    stack = []
    n = root
    while n != None or len(stack) > 0:
        while n:
            stack.append(n)
            n = n["left"]

        n = stack.pop()
        print(n["val"])
        
        n = n["right"]

def dfs_postorder(root):
    stack1 = []
    stack2 = []
    n = root
    stack1.append(n)
    while len(stack1) > 0:
        n = stack1.pop()
        stack2.append(n)

        if n["left"]:
            stack1.append(n["left"])
        if n["right"]:
            stack1.append(n["right"])

    while len(stack2) > 0:
        n = stack2.pop()
        print(n["val"])
    


def main():
    jstr = sys.stdin.read().strip()
    node = json.loads(jstr)
    print(node)

    print("Preorder")
    dfs_preorder(node)
    print("Inorder")
    dfs_inorder(node)
    print("Postorder")
    dfs_postorder(node)

if __name__ == "__main__":
    sys.exit(main())
