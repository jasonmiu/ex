#!/usr/bin/env python3

import sys

class Node(object):
    def __init__(self, c):
        self.val = c
        self.next_chars = dict()

def trie_add_word(root, word):
    node = root
    for c in word:
        if c in node.next_chars:
            node = node.next_chars[c]
        else:
            new_node = Node(c)
            node.next_chars[c] = new_node
            node = new_node

    # "!" is the terminal indicator, for example:
    # a-p-p-l-e
    #         +-b-e-r-r-y
    #         +-j-a-c-k
    # the word "apple" is prefix of "appleberry" and "applejack"
    # if we search "apple" without terminal indicator,
    # the word "apple" will not appear since it in the middle of
    # tree. Add a special node with "!", so:
    # a-p-p-l-e-!
    #         +-b-e-r-r-y-!
    #         +-j-a-c-k-!
    # we can get a complete word when we see a child node with
    # "!" terminal indicator
    node.next_chars["!"] = Node("!")

    return root

def print_trie(root, level):
    indent = " " * (2 * level)
    print("{}{}".format(indent, root.val))
    for (c, n) in  root.next_chars.items():
        print_trie(n, level + 1)

    
def dfs(root, s, array):
    
    if root.val == "!":
        array.append(s)
    else:
        for (c, n) in root.next_chars.items():
            if c != "!":
                dfs(n, s + c, array)
            else:
                # next child node is a terminal indicator
                # do not include the "!"
                dfs(n, s, array)
    return

def get_all_possibles(root, prefix):
    n = root
    p = ""
    for c in prefix:
        if c in n.next_chars:
            n = n.next_chars[c]
            p = p + c
        else:
            n = None
            break

    array = []
    if n:
        dfs(n, p, array)

    return array


def main():

    if len(sys.argv) != 3:
        print("Search all possible strings with a prefix using trie.")
        print("Usage: {} <file with strings> <prefix to search>".format(sys.argv[0]))
        return 1
    
    fd = open(sys.argv[1], 'r')

    trie_root = Node("")
    
    line = fd.readline().strip()
    while(line):
        trie_add_word(trie_root, line)
        
        line = fd.readline().strip()

    for s in get_all_possibles(trie_root, sys.argv[2]):
        print(s)
    

if __name__ == "__main__":
    sys.exit(main())
