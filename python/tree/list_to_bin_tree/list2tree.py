#!/usr/bin/env python3

import sys
import queue
import json

class Node(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

    def _to_json(self):
        d = {}
        d["val"] = self.val
        if self.left:
            d["left"] = self.left._to_json()
        else:
            d["left"] = None

        if self.right:
            d["right"] = self.right._to_json()
        else:
            d["right"] = None
            
        return d

    def to_json(self):
        d = self._to_json()
        return json.dumps(d)

        

def preorder_print(root, indent):
    if root:
        pad = ' ' * indent
        print("{}{}".format(pad, root.val))
        preorder_print(root.left, indent+2)
        preorder_print(root.right, indent+2)

    return

import argparse

def main():

    help_str = \
'''
Output binary tree from a leetcode-like list format.
For a tree:
        3
       / \\
      6   2
     /   /
    9   10
Example input: '3 6 2 9 None 10'
'''    
    
    ap = argparse.ArgumentParser(description=help_str, formatter_class=argparse.RawTextHelpFormatter)

    ap.add_argument('-j', action="store_true", default=False, help="output json. Preorder print out from left if this option is false (default=false)")
    ap.add_argument('-v', action="store_true", default=False, help="verbose, use for debugging")
    ap.add_argument('rest', nargs=argparse.REMAINDER, help="space separated string as tree nodes")
    args = ap.parse_args(sys.argv[1::])

    if len(args.rest) != 1:
        ap.print_help()
        return 1
    
    arr = args.rest[0].split(' ')
    
    if len(arr) == 0:
        return 0

    root = Node(arr[0])
    i = 1
    q = queue.Queue()
    q.put(root)

    while i < len(arr):
        parent = q.get()
        left_child = None
        right_child = None

        nulls = ["none", "nil", "null"]
        nulls = nulls + [s.capitalize() for s in nulls]

        if arr[i] not in nulls:
            left_child = Node(arr[i])
            q.put(left_child)
        i += 1
        if i < len(arr) and arr[i] not in nulls:
            right_child = Node(arr[i])
            q.put(right_child)
        i += 1

        if (args.v):
            print("parent", parent.val)
            if left_child:
                print("  left", left_child.val)
            else:
                print("  left", "null")
            if right_child:
                print("  right", right_child.val)
            else:
                print("  right", "null")
            
        parent.left = left_child
        parent.right = right_child

    if not args.j:
        preorder_print(root, 0)
    else:
        print(root.to_json())

    return 0


if __name__ == "__main__":
    sys.exit(main())
