#!/usr/bin/env python3

import sys

def lowest_set_bit(n):
    return n & ~(n - 1)

def clear_lowest_set_bit(n):
    return n & (n - 1)

# The idea is to make sure all the right hand side (lesser significant bits)
# are set. Then + 1 to propagate the carry to the left bit originally enabled
# left most bit, then we shift >> back 1 bit to get back the originally set bit
# This only work for fixed width int
def highest_set_bit_32(n):
    # example, n = b1010
    n = n | n >> 1 # 1010 | 0101 => 1101
    n = n | n >> 2 # 1101 | 0011 => 1111
    n = n | n >> 4
    n = n | n >> 8
    n = n | n >> 16 # Every time we set half of bits, so >> 16 is enough for 32bits
    n = n + 1
    return n >> 1
    

def swap_bits(n, i, j):
    if i == j:
        return n

    i_mask = 1 << i
    j_mask = 1 << j

    # only need to swap when n[i] and n[j] are diff
    # swap means we can flip the n[i] and n[j]
    if (n >> i & 0x1) != (n >> j & 0x1):
        return (i_mask | j_mask) ^ n
    else:
        return n

# True if the interger n has only 1 bit set
# Also can use to test if n is a power of 2
def has_only_1bit_set(n):
    if ((n - 1) & n) == 0:
        return True
    else:
        return False

def main():
    n = int(sys.argv[1])

    print("lowest set bit of {0}: {1} (b{1:b})".format(n, lowest_set_bit(n)))
    print("clear lowest set bit of {}: {}".format(n, clear_lowest_set_bit(n)))
    print("highest set bit of a 32bits interger {0} (b{0:b})is {1} (b{1:b})".format(n, highest_set_bit_32(n)))
    print("swap bit 0 and 3 of {0} (b{0:016b}): {1} (b{1:016b})".format(n, swap_bits(n, 0, 3)))
    print("{0} (b{0:016b}) has only 1 bit set: {1}".format(n, has_only_1bit_set(n)))


    return 0

if __name__ == "__main__":
    sys.exit(main())
