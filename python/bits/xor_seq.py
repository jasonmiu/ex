#!/usr/bin/env python3

# a experiment triggered by
# 1442. https://leetcode.com/problems/count-triplets-that-can-form-two-arrays-of-equal-xor/
# If a sequence of number XOR-ed together and the result is 0,
# is cutting any point in this sequence gives the same values of the XOR-ed 1st and 2nd halves?
# XOR-ing seq "2 3 4 5 6 7 8 9" gives 0.

import sys

def main():

    if len(sys.argv) != 3:
        print("{} <quoted list of int> <cutting index>".format(sys.argv[0]))
        return 1

    nums = list(map(int, sys.argv[1].split()))
    cut_idx = int(sys.argv[2])

    for (i, n) in enumerate(nums):
        print("{}|{}| b{:016b}".format(i, n, n))


    x = nums[0]
    for i in range(1, len(nums)):
        x = x ^ nums[i]
    print("XOR altogether {}".format(x))
    print("If XOR-ing all numbers resulting 0, any cutting index will have the same values of XOR-ed 1st half and XOR-ed 2nd half")
    

    print("XOR from 0 -> {}".format(cut_idx-1))
    x = nums[0]
    for i in range(1, cut_idx):
        x = x ^ nums[i]
    print("b{:016b}".format(x))

    print("XOR from {} -> {}".format(cut_idx, len(nums)-1))
    x = nums[cut_idx]
    for i in range(cut_idx+1, len(nums)):
        x = x ^ nums[i]
    print("b{:016b}".format(x))


    print("Move cut index from {} to {}".format(cut_idx, cut_idx-1))

    x = nums[0]
    for i in range(1, cut_idx-1):
        x = x ^ nums[i]
    print("XOR from {} -> {}".format(0, cut_idx-2))
    print("b{:016b}".format(x))

    
    x = nums[cut_idx-1]
    for i in range(cut_idx, len(nums)):
        x = x ^ nums[i]
    print("XOR from {} -> {}".format(cut_idx-1, len(nums)-1))
    print("b{:016b}".format(x))
        
    return 0

if __name__ == "__main__":
    sys.exit(main())
