#!/usr/bin/env python3

import sys
import collections
import math
from queue.priority_queue import PriorityQueue


class Graph(object):
    def __init__(self):
        self.start = None
        self.end = None
        self.adj_list = collections.defaultdict(list)
        self.visited = set()
        self.distances = dict()
        self.paths = dict()

    def init_single_source(self):
        for u in self.adj_list:
            self.distances[u] = math.inf
            self.paths[u] = None

        self.distances[self.start] = 0

    def shortest_path(self):
        self.init_single_source()

        q = PriorityQueue()
        for u in self.adj_list:
            q.add(u, self.distances[u])
            

        while len(q) > 0:
            u = q.pop()
            
            self.visited.add(u)
            edges = self.adj_list[u]
            for (v, w) in edges:
                if self.distances[v] > self.distances[u] + w:
                    self.distances[v] = self.distances[u] + w
                    self.paths[v] = u
                    if v in q:
                        q.add(v, self.distances[v])
                    

#        print(self.distances)

        shortest_path_vertices = []
        v = self.end
        shortest_path_vertices.append(v)
        while v != self.start:
            v = self.paths[v]
            shortest_path_vertices.append(v)


        shortest_path_vertices.reverse()
        return (self.distances[self.end], shortest_path_vertices)
    
    def print(self):
        for u in self.adj_list:
            for (v, w) in self.adj_list[u]:
                print("{} -- {} --> {}".format(u, w, v))

def read_graph(f):
    fd = open(f, "r")
    g = Graph()
    
    line = fd.readline().strip()
    while line.startswith("#") or len(line) == 0:
        line = fd.readline().strip()
    
    (s, e) = line.split()
    g.start = s
    g.end = e
    while line:
        line = fd.readline().strip()
        if not line:
            break
        if line.startswith("#") or len(line) == 0:
            continue

        (u, v, w) = line.split()
        w = float(w)
        g.adj_list[u].append((v, w))
        if v not in g.adj_list:
            g.adj_list[v] = []

    return g


def main():
    infile = sys.argv[1]

    g = read_graph(infile)
    g.print()
    (dist, path) = g.shortest_path()
    print("dist from {} -> {} = {}".format(g.start, g.end, dist))
    for v in path:
        print(v, end='')
        if v != path[-1]:
            print(" -> ", end="")
    print()

    return 0


if __name__ == "__main__":
    sys.exit(main())
        

    
