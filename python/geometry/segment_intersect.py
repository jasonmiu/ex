#!/usr/bin/env python3

# https://bryceboe.com/2006/10/23/line-segment-intersection-algorithm/

class Point:
        def __init__(self,x,y):
                self.x = x
                self.y = y
        def __str__(self):
                return "({}, {})".format(self.x, self.y)

'''
The solution involves determining if three points are listed in a
counterclockwise order. So say you have three points A, B and C. If
the slope of the line AB is less than the slope of the line AC then
the three points are listed in a counterclockwise order.  '''
def ccw(A,B,C):
        return (C.y-A.y)*(B.x-A.x) > (B.y-A.y)*(C.x-A.x)

'''
Think of two line segments AB, and CD. These intersect if and only if
points A and B are separated by segment CD and points C and D are
separated by segment AB. If points A and B are separated by segment CD
then ACD and BCD should have opposite orientation meaning either ACD
or BCD is counterclockwise but not both. Therefore calculating if two
line segments AB and CD intersect is as follows: '''

def intersect(A,B,C,D):
        return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)


a = Point(0,0)
b = Point(0,1)
c = Point(1,1)
d = Point(1,0)

print("A", a)
print("B", b)
print("C", c)
print("D", d)

print("Intersect of AB CD", intersect(a,b,c,d))
print("Intersect of AC DB", intersect(a,c,b,d))
print("Intersect of AD BC", intersect(a,d,b,c))
