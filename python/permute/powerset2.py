import sys

ans = []

def powerset(elements, build_sets, idx):
    if idx >= len(elements):
        ans.extend(build_sets)
        return

    extend_sets = build_sets.copy()
    for s in build_sets:
        ss = s + [elements[idx]]
        extend_sets.append(ss)
    powerset(elements, extend_sets, idx+1)

def main():
    elements = list(map(int, sys.argv[1::]))

    # Another algorithm of powerset.
    #
    # build_sets is the sets we are building up
    # start with 1 empty set
    # for each element in the input,
    # add it to the each set in the current build_sets
    # The idea is for each element [i], we can assume
    # the current build_sets has the power set of i-1 already
    # so we can add the element [i] to each set in the current
    # power sets of i-1, add the result to the new build_sets,
    # so the new build_sets now has the power sets of i.
    # if we have K sets of i-1, we add [i] to K sets, that will
    # create more K sets, so the new build_sets will have
    # K*2 sets. For total n input elements, the number of the sets
    # in the resulted power set is 2^n.

    build_sets = [ [] ]
    powerset(elements, build_sets, 0)

    for i in ans:
        print(i)


if __name__ == "__main__":
    sys.exit(main())
