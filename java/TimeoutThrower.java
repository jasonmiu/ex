import java.lang.*;
import java.util.concurrent.*;

public class TimeoutThrower {
    public static void timeout_me(int t) throws Exception {
        System.out.println("sleep " + t + " ...");
        try {
            Thread.sleep(t);
        } catch (Exception e) {
            System.out.println("Sleep raised exception " + e);
        }
        System.out.println("Now throw exception");
        throw new TimeoutException();
    }
 
    public static void main(String args[]) throws Exception {
        int t = Integer.parseInt(args[0]);
        if (t > 0) {
            timeout_me(t);
        }

        System.out.println("main() finished");
    }
}
