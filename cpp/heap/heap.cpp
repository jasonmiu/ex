#include <iostream>
#include <iterator>
#include <vector>
#include <stdio.h>

using namespace std;

template <class T>
class MaxHeap
{
private:
    vector<T> data;
    void heapify_at(int i);

public:
    MaxHeap() {};
    MaxHeap(vector<T> &v);

    T operator[](int i) const;
    int size() const;

    void heapify();
    T pop();
};

template <class T>
MaxHeap<T>::MaxHeap(vector<T> &v)
{
    this->data = v;
}

template <class T>
T MaxHeap<T>::operator[](int i) const
{
    return data[i];
}

template <class T>
ostream &operator<<(ostream &outs, const MaxHeap<T> &h)
{
    outs << "[";
    for (int i=0; i < h.size(); i++) {
        outs << h[i];
        if (i < h.size() - 1) {
            outs << ",";
        }
    }
    outs << "]";
    return outs;
}

template <class T>
int MaxHeap<T>::size() const
{
    return data.size();
}

template <class T>
void MaxHeap<T>::heapify_at(int i)
{
    int n = this->data.size();
    int l_idx = (i * 2) + 1;
    int r_idx = (i * 2) + 2;

    int max_idx = i;

    if ((l_idx < n) && (this->data[l_idx] > this->data[i])) {
        max_idx = l_idx;
    }

    if ((r_idx < n) && (this->data[r_idx] > this->data[max_idx])) {
        max_idx = r_idx;
    }

    if (max_idx != i) {
        T temp = this->data[i];
        this->data[i] = this->data[max_idx];
        this->data[max_idx] = temp;

        return this->heapify_at(max_idx);
    }

    return;
}

template <class T>
void MaxHeap<T>::heapify()
{
    int n = this->data.size();

    for (int i = n/2; i >= 0; i--) {
        this->heapify_at(i);
    }

    return;
}

template <class T>
T MaxHeap<T>::pop()
{
    T m = this->data[0];

    int n = this->data.size();
    this->data[0] = this->data[n-1];
    this->data.erase(this->data.end() - 1);
    this->heapify_at(0);

    return m;
}



int main(int argc, char *argv[])
{
    vector<int> v;

    for (int i=1; i < argc; i++) {
        int d = atoi(argv[i]);
        v.push_back(d);
    }

    MaxHeap<int> h(v);
    h.heapify();

    cout << h << endl;

    int n = h.size();
    for (int i=0; i < n; i++) {
        cout << "Pop " <<  h.pop() << endl;
    }

    return 0;
}
