#include <vector>
#include <iostream>
#include <array>
#include <utility>

using namespace std;

const int N = 5;

struct IntPair {
    int a[2];
};

int main()
{
    vector<IntPair> va;

    cout << "vector<IntPair> where IntPair is a struct of int a[]\n";
    for(int i=0; i < N; i++) {
        IntPair a = {i, i*2};
        va.push_back(a);
    }

    for(int i=0; i < N; i++) {
        cout << va[i].a[0] << " " << va[i].a[1] << "\n";
    }

    vector<array<int, 2>> va2;
    cout << "using STL array<int, 2> as element of vector\n";
    for(int i=0; i < N; i++) {
        array<int, 2> a = {i, i*2};
        va2.push_back(a);
    }

    for(int i=0; i < N; i++) {
        cout << va2[i][0] << " " << va2[i][1] << "\n";
    }

    vector<pair<int, int>> va3;
    cout << "using STL pair<int, int> as element of vector\n";
    for(int i=0; i < N; i++) {
        pair<int, int> a = {i, i*2};
        va3.push_back(a);
    }

    for(int i=0; i < N; i++) {
        cout << va3[i].first << " " << va3[i].second << "\n";
    }
    

    return 0;
    
}
