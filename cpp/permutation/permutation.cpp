#include <iostream>
#include <unordered_set>
#include <vector>
#include <algorithm>

using namespace std;

void permutate(const vector<int>& a, unordered_set<int>& s, vector<int>& cur, vector<vector<int>>& result)
{
    if (cur.size() == a.size()) {
        result.push_back(cur);
        return;
    }

    for (int i : a) {
        if (s.find(i) == s.end()) {
            continue;
        }
        cur.push_back(i);
        s.erase(i);

        permutate(a, s, cur, result);

        cur.pop_back();
        s.insert(i);
    }

    return;
}


int main(int argc, char *argv[])
{
    if (argc <= 1) {
        cout << "Generate permutations of the given arguments. (Using generation algor)\n";
        cout << "Usage: " << argv[0] << " <int> [[int] [int] ...] \n";
        return -1;
    }

    vector<int> nums;
    for(int i = 1; i < argc; i++) {
        nums.push_back(atoi(argv[i]));
    }

    vector<vector<int>> result;
    unordered_set<int> s(nums.begin(), nums.end());
    vector<int> cur;

    permutate(nums, s, cur, result);
    for (auto v : result) {
        cout << "[ ";
        for (auto i : v) {
            cout << i << " ";
        }
        cout << "]\n";
    }

    return 0;
}
