#include <algorithm>
#include <vector>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int bisect_left(const vector<int>& nums, int target)
{
    int start = 0;
    // NOTE:
    // end is n, not n - 1,
    // because this function can return n (size of nums) as the bisection
    // index, when the target is the larger than all elements in nums.
    // The 'end' here specified the search space, which can be n (1 more index
    // than original input nums).
    int end = nums.size();

    while (start < end) {
        int mid = start + ((end - start) / 2);

        if (target <= nums[mid]) {
            end = mid;
        } else {
            start = mid + 1;
        }
    }

    return start;
}

int main(int argc, char* argv[])
{

    if (argc < 3) {
        printf("Using bisection to find the left insertion point of target value to a sorted list\n");
        printf("%s <target> <int> [int] [int] ... [int]\n", argv[0]);
        return 1;
    }

    vector<int> nums;

    int target = 0;
    char *non_int_ptr = NULL;
    int a = strtol(argv[1], &non_int_ptr, 10);
    if (non_int_ptr == argv[1]) {
        printf("Unknown target value %s\n", argv[1]);
        return 1;
    } else {
        target = a;
    }

    for (int i = 2; i < argc; i++) {
        char *non_int_ptr = NULL;
        int a = strtol(argv[i], &non_int_ptr, 10);
        if (non_int_ptr == argv[i]) {
            printf("Unknown value %s\n", argv[i]);
            return 1;
        }

        nums.push_back(a);
    }

    printf("Target: %d\n", target);

    sort(nums.begin(), nums.end());

    int bi_idx = bisect_left(nums, target);

    auto itr = lower_bound(nums.begin(), nums.end(), target);
    int lower_bound_idx = itr - nums.begin();

    printf("Left insertion point: %d, using lower_bound(): %d\n", bi_idx, lower_bound_idx);
    if(lower_bound_idx != bi_idx) {
        printf("The bisect_left result does not match lower_bound()!\n");
        return 1;
    }

    return 0;
}
