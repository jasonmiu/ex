#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <errno.h>

using namespace std;

int partition(vector<int>& nums, int start, int end)
{
    int pivot = nums[end];
    int i = start;
    int left_of_pivot = start;

    while (i <= end-1) {
        if (nums[i] <= pivot) {
            swap(nums[i], nums[left_of_pivot]);
            left_of_pivot ++;
        }
        i++;
    }

    swap(nums[i], nums[left_of_pivot]);
    return left_of_pivot;
}

int main(int argc, char *argv[])
{
    vector<int> nums;

    if (argc <= 1) {
        printf("Do a partition algorithm on a list of integers\n");
        printf("The last value in the list will be the pivot, after partition, the elements smaller than or equal to it will be on its left.\n");
        printf("%s i [i_1] [i_2] ... [i_n]\n", argv[0]);
        return 1;
    }

    for (int i = 1; i < argc; i++) {
        char* non_num_ptr = NULL;
        long long a = strtol(argv[i], &non_num_ptr, 10);
        if (non_num_ptr == argv[i] || errno == ERANGE) {
            printf("Unknown value: %s\n", argv[i]);
            return 1;
        }

        nums.push_back(a);
    }

    int pivot_idx = partition(nums, 0, nums.size()-1);

    printf("Pivot idx = %d, pivot = %d\n", pivot_idx, nums[pivot_idx]);
    for (int a : nums) {
        printf("%d ", a);
    }
    printf("\n");
    return 0;
}
