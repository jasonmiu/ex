#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

extern const int kTheNumber = 42;
extern const char kTheCString[] = "This is c string";

// Appendix C (C++11, C.1.2) gives the rationale
//     Change: A name of file scope that is explicitly declared const,
//     and not explicitly declared extern, has internal linkage, while in
//     C it would have external linkage
//
//     Rationale: Because const objects can be used as compile-time
//     values in C++, this feature urges programmers to provide explicit
//     initializer values for each const. This feature allows the user to
//     put const objects in header files that are included in many
//     compilation units.
const int kAnotherNumber = 99;

// Non const var, ext linkage
int kMoreNumber = 101;


// Non-const global variables have external linkage by default
// Const global variables have internal linkage by default
// Functions have external linkage by default
void print_const(void);


