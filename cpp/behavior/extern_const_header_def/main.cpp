#include <iostream>

// Without extern here, both function and variable
// will give error during *compilation* of this
// translation unit
extern void print_const(void);
extern const int kTheNumber;

// kAnotherNumber has internal linkage
//extern int kAnotherNumber;

extern int kMoreNumber;

int main(int argc, char* argv[])
{
    print_const();

    std::cout << "From main, kTheNumber: " << kTheNumber << "\n";
//    std::cout << "From main, kAnotherNumber: " << kAnotherNumber << "\n";
    std::cout << "From main, kMoreNumber: " << kMoreNumber << "\n";

    return 0;
}
