#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <memory>

using namespace std;

class Base
{
public:
    virtual void do_thing() {
        cout << "Base do thing\n";
    }
};

class Child : public Base
{
public:
    static unique_ptr<Child> create() {
        return unique_ptr<Child>(new Child());
    };

    void do_thing() {
        cout << "Child do thing\n";
    }
};

int main(int argc, char *argv[])
{
    auto x = unique_ptr<Base>(new Base());
    unique_ptr<Base> a = unique_ptr<Base>(new Child());
    unique_ptr<Base> b = make_unique<Child>();
    unique_ptr<Base> c = Child::create();

    cout << R"(auto x = unique_ptr<Base>(new Base());)" << "\n";
    x->do_thing();

    cout << R"(unique_ptr<Base> a = unique_ptr<Base>(new Child());)" << "\n";
    a->do_thing();

    cout << R"(unique_ptr<Base> b = make_unique<Child>();)" << "\n";
    b->do_thing();

    cout << R"(unique_ptr<Base> c = Child::Create();)" << "\n";
    c->do_thing();

    return 0;
}
