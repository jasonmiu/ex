#include <iostream>

/*
 * https://stackoverflow.com/questions/66388076/why-does-not-c-template-generate-duplicate-symbols-when-it-is-included-by-muti
 * My question about "Why does not c++ template generate duplicate symbols when it is included by mutiple source files?"
 */

template<typename T>
void print(T msg)
{
    std::cout << "From Template print:" << msg << "\n";
}

#if 0
void samefunc(void)
{
    return;
}
#endif
