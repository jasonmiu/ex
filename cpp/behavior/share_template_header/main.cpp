#include <string>

extern void a_func(const std::string& s);
extern void b_func(const std::string& s);

int main(int argc, char *argv[])
{

    a_func("Call from MAIN to A");
    b_func("Call from MAIN to B");
    return 0;
}
