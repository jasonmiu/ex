/*
 * Try to understand the "temporary" copy during the function return.
 * Check how the copy elision and return value optimatiztion work.
 */


#include <iostream>
#include <stdio.h>
#include <stdlib.h>

struct Thing
{
    Thing() {
        printf("== Created a thing %p\n", this);
    };

    Thing(struct Thing& t) {
        printf("== Copied a thing %p -> %p\n", &t, this);
    }

    ~Thing() {
        printf("== Destoried a thing %p\n", this);
    }
};


Thing ret_thing()
{
    printf("Just create a thing a thing and return from function\n");
    Thing t;

    return t;
}

void intake_thing(Thing t)
{
    printf("Function intake a pass-by-value\n");
    return;
}

void intake_thing_ref(Thing& t)
{
    printf("Function intake a pass-by-ref\n");
    return;
}

Thing& pass_thing_ref(Thing& t)
{
    printf("Function intake a pass-by-ref and return it back as ref\n");
    return t;
}


int main(void)
{
    printf("Define a thing\n");
    Thing thing;

    printf("Call ret_thing()\n");
    thing = ret_thing();

    printf("Call intake_thing()\n");
    intake_thing(thing);

    printf("Define a thing t1\n");
    Thing t1;
    printf("Define a ref to t1\n");
    Thing& tr = t1;

    printf("Call intake_thing_ref\n");
    intake_thing_ref(tr);

    printf("Define a thing2\n");
    Thing thing2;

    printf("Call pass_thing_ref on thing1 ref, and assign return to thing2\n");
    thing2 = pass_thing_ref(tr);

    printf("Call ret_thing(), assign the returned object to thing2\n");
    thing2 = ret_thing();

    printf("Main end\n");

    return 0;
}
