#include <vector>
#include <string>
#include <iterator>
#include <iostream>
#include <sstream>

using namespace std;

int main()
{
    string s = "This is some words, can we split?";

    std::istringstream in_stream(s);

    // create a iterator of the beginning of stream
    std::istream_iterator<string> s_iter = istream_iterator<string>(in_stream);
    // istream_iterator() construct the end of stream
    std::istream_iterator<string> end_iter = istream_iterator<string>();
    
    // vector (InputIterator first, InputIterator last)
    std::vector<string> tokens(s_iter, end_iter);

    for (int i=0; i < tokens.size(); i++) {
        cout << tokens[i] << "\n";
    }

    string ss = "This;is;some;words,;can;we;split?";
    in_stream = istringstream(ss); // using '=' for istringstream is c++11
    tokens = vector<string>();
    string t;
    while (getline(in_stream, t, ';')) {
        tokens.push_back(t);
    }
    for (int i=0; i < tokens.size(); i++) {
        cout << tokens[i] << "\n";
    }
        

    return 0;
        
}
