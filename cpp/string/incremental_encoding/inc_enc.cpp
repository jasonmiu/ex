#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

using namespace std;

int prefix_len(string a, string b)
{
    int b_len = b.length();
    int p = 0;
    for (int i=0; i < a.length(); i++) {
        if (i >= b_len) {
            break;
        }

        if (a[i] == b[i]) {
            p++;
        } else {
            break;
        }
    }

    return p;
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        cerr << "Usage: " << argv[0] << " <strings file>\n";
        return 1;
    }

    
    fstream fs;
    string w;
    fs.open(argv[1], ios::in);
    if (fs.fail()) {
        cerr << "Failed to open file " << argv[1] << "\n";
        return 1;
    }
    
    string last = "";
    while (getline(fs, w)) {
        int p = prefix_len(last, w);
        string suffix = w.substr(p);
        cout << p << " " << suffix << "\n";
        last = w;
    }

    return 0;
}

