#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

using namespace std;

int main(int argc, char *argv[])
{
    if (argc != 2) {
        cerr << "Usage: " << argv[0] << " <encoded file>\n";
        return 1;
    }
    
    fstream fs;
    fs.open(argv[1], ios::in);
    if (fs.fail()) {
        cerr << "Failed to open file " << argv[1] << "\n";
        return 1;
    }
    
    int p;
    string suffix;
    string line;
    string last = "";
    while (getline(fs, line)) {
        stringstream ss(line);
        ss >> p;
        ss >> suffix;

        string prefix = last.substr(0, p);
        string w = prefix + suffix;
        last = w;
        cout << w << "\n";
    }

    return 0;
    
}
