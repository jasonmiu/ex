#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <algorithm>

// compile: g++ -o qsort qsort.cpp

using namespace std;


void qsort(int *arr, int start, int end, int (*part_func)(int*, int, int))
{
    if (start < end) {
        int p_idx = part_func(arr, start, end);
        qsort(arr, start, p_idx, part_func);
        qsort(arr, p_idx + 1, end, part_func);
    }

    return;
}

int part(int *arr, int start, int end)
{
    int p = arr[start];
    int i = start - 1 ;
    int j = end + 1;
    while (1) {
        i = i + 1;
        while (arr[i] < p) {
            i = i + 1;
        } // i stops at arr[i] >= p

        j = j - 1;
        while (arr[j] > p) {
            j = j - 1;
        } // j stops at arr[j] <= p

        if (i < j) {
            int t = arr[i];
            arr[i] = arr[j];
            arr[j] = t;
        } else {
            break;
        }
    }

    return j;
}

int lomuto_part(int *arr, int start, int end)
{
    int p = arr[end];

    int i = start - 1;
    // two known regions
    // A[start -> i] is the region with known values <= p
    // A[i+1 -> j] is the region with known values > p
    // the region from A[j -> end] is unknown values

    for (int j = start; j <= end-1; j++) {
        if (arr[j] <= p) {
            i = i + 1;
            int t = arr[i];
            arr[i] = arr[j];
            arr[j] = t;
        }
    }

    i = i + 1;
    int t = arr[i];
    arr[i] = arr[end];
    arr[end] = t;

    if (i < end)
        return i;
    else
        // if i >= end, means the pivot value is the largest one,
        // and it will be at the end of array, its index will be returned
        // to qsort() as p_index and becomes the first partition for
        // qsort() again. Since it is already the largest number and the
        // 'end' argument as the p_index will always be the same. Handle
        // this special case for skipping the largest value in the array
        // as we know it is at the right place
        return i - 1;
}

int main(int argc, char *argv[])
{

    if (argc != 3) {
        printf("Usage: %s <number of elements> <algorithm>\n", argv[0]);
        printf("<algorithm>:\n");
        printf("1: two ends partition qsort\n");
        printf("2: lomuto parition qsort\n");
        printf("3: STL sort\n");
        return 1;
    }
    
    int arr_nr = atoi(argv[1]);
    int algo = atoi(argv[2]);

    int *a;

    a = new int[arr_nr];

    srand(0);
    for (int i = 0; i < arr_nr; i++) {
        a[i] = rand() % arr_nr;
    }

    int p = lomuto_part(a, 0, arr_nr-1);


    switch (algo) {
    case 1:
        qsort(a, 0, arr_nr-1, &part);
        break;
    case 2:
        qsort(a, 0, arr_nr-1, &lomuto_part);
        break;
    case 3:
        sort(a, a+arr_nr);
        break;
    }

    for (int i = 0; i < arr_nr; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");

    delete[] a;

    return 0;
}
