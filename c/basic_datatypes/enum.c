#include <stdio.h>
#include <stdlib.h>

// If just define the enum values as
// RED, GREEN, and do the same enum values in
// 'enum Traffic', will have a error
// "redefinition of enumerator 'GREEN'"
// for the same enum value.
enum Color
{
    C_RED,
    C_GREEN,
    C_BLUE
};

enum Traffic
{
    T_RED,
    T_YELLOW,
    T_GREEN
};

int main(void)
{
    enum Color c_r = C_RED;
    enum Color c_g = C_GREEN;
    enum Traffic t_r = T_RED;
    enum Traffic t_g = T_GREEN;

    printf("Color RED %d, GREEN %d\n", c_r, c_g);
    printf("Traffic RED %d, GREEN %d\n", t_r, t_g);

    return 0;
}
