
#include <stdio.h>
#include <stdlib.h>

// Looks clearer as
// typedef void (*)(int) INT_FP
// a new type INT_FP is defined as "void (*)(int)"
//
typedef void (*INT_FP) (int);

// typedef void (*INT_FP) (int);
//         \      \_ Label    |
//          \_________________|__ Native
//
// typedef will replace the Labels the variables defined with it,
// along with the Native part.
// For a function pointer, the native definition is
// (ret type) (* func_name) (arg type)
// Hence for a declearion:
// INT_FP fp;
// The "fp" will replace the INT_FP in the typedef string, becomes
// void (*fp) (int);
// which decleared a function point that returns a void and intakes a int


void my_fp(int x)
{
  printf("In Function Pointer get %d\n", x);

  return;
}


int main(void)
{

  // The type of function point fp is 
  // void (*) (int)
  // Read as: 
  // A function pointer (*) named as "fp" that returns void
  // and intakes a int

  void (*fp) (int);


  INT_FP fp2;

  fp = &my_fp;
  fp2 = &my_fp;


  fp(10);
  fp2(20);

  return 0;
}
