#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

void vf(int cnt, ...)
{
    va_list arglist;

    va_start(arglist, cnt);

    int i = 0;
    for(i=0; i<cnt; i++) {
        int d = va_arg(arglist, int);
        printf("%d\n", d);
    }

    for(i=0; i < 10; i++) {
        int x = va_arg(arglist, int);
        printf("Over the va_list size %d, read back X: %d\n", i+1, x);
    }

    va_end(arglist);

    return;
}

int main()
{
    printf("Call variadic c function with 3 (1, 2, 3) varargs\n");
    vf(3, 1, 2, 3);
}
